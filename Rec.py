#rectangle class
class Rectangle:
    #rectangle constructor
    def __init__(self,width=1,height=1):
        self.width=width
        self.height=height

    #function to get the width
    def getWidth(self):
        return self.width  

    #function to get the Height
    def getHeight(self):
        return self.height         

    #function to get the area
    def getArea(self):
        area=self.width*self.height
        return round(area,1) 

    #function to get the perimeter 
    def getPerimeter(self):
        perimeter=self.width+self.height
        return  perimeter  

#creating the x rectangle
x= Rectangle(4,40)
#creating the y recrangle
y= Rectangle(3.5,35.9)
#creating the default rectangle
z=Rectangle()
#print x rectangle
print("Rectangle X is: \nWidth=",x.getWidth(),"\nHeight=",x.getHeight(),"\nArea is=", x.getArea(),"\nPerimeter is=",x.getPerimeter())
#print y rectangle
print("\nRectangle Y is: \nWidth=",y.getWidth(),"\nHeight=",y.getHeight(),"\nArea is=", y.getArea(),"\nPerimeter is=",y.getPerimeter())
#print z rectangle
print("\nRectangle Z is: \nWidth=",z.getWidth(),"\nHeight=",z.getHeight(),"\nArea is=", z.getArea(),"\nPerimeter is=",z.getPerimeter())
